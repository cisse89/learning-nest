import 'reflect-metadata';
import { NotFoundException } from '@nestjs/common/exceptions';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;
  let fakeAuthService: Partial<AuthService>;
  let fakeUsersService: Partial<UsersService>;

  beforeEach(async () => {
    fakeAuthService = {
      signup: (email: string, password: string) =>
        Promise.resolve({ id: 1, email, password } as User),
      signin: (email: string, password: string) =>
        Promise.resolve({ id: 1, email, password } as User),
    };

    fakeUsersService = {
      create: (email, password) => {
        return Promise.resolve({ id: 1, email, password } as User);
      },
      findUser: (id: number) => {
        return Promise.resolve({
          id,
          email: 'testmail',
          password: 'testpass',
        } as any);
      },
      find: (email: string) => {
        return Promise.resolve([
          { id: 1, email: 'testmail', password: 'testpass' } as User,
        ]);
      },
      update(id, attrs) {
        return Promise.resolve({ id, ...attrs } as User);
      },
      removeUser(id) {
        return Promise.resolve({} as User);
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: AuthService, useValue: fakeAuthService },
        { provide: UsersService, useValue: fakeUsersService },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAllUsers return a list of users with given email', async () => {
    const users = await controller.findUsersWithMail('abc');
    expect(users.length).toEqual(1);
  });

  it('findUser returns single user with given id', async () => {
    const user = await controller.findUser('1');
    expect(user).toBeDefined();
  });

  it('findUser throws when user with id not found', async () => {
    fakeUsersService.findUser = (id: number) => null;
    await expect(controller.findUser('1')).rejects.toThrow(NotFoundException);
  });

  it('signin should correctly add userId to session', async () => {
    const session = {userId:-9999};
    const user = await controller.logIn(
      {
        email: 'test',
        password: 'pass',
      },
      session,
    );

    expect(user.id).toEqual(1);
    expect(user.id).toEqual(session.userId);
  });
});
