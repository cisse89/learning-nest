import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { randomBytes, scrypt } from 'crypto';
import { promisify } from 'util';
import { UsersService } from './users.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async signup(email: string, password: string) {
    const users = await this.usersService.find(email);
    if (users.length > 0) {
      throw new BadRequestException('email in use');
    }

    const salt = randomBytes(8).toString('hex');
    const hashedPass = (await promisify(scrypt)(password, salt, 64)) as Buffer;
    const result = salt + '.' + hashedPass.toString('hex');

    const user = await this.usersService.create(email, result);
    return user;
  }

  async signin(email: string, password: string) {
    const [user] = await this.usersService.find(email);
    if (!user) {
      throw new NotFoundException('user not found');
    }

    const [salt, storedPass] = user.password.split('.');
    const hashedPass = (await promisify(scrypt)(password, salt, 64)) as Buffer;

    if (storedPass !== hashedPass.toString('hex')) {
      throw new ForbiddenException('bad login/password');
    }

    return user;
  }
}
