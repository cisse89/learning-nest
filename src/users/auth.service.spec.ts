import {
  BadRequestException,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let fakeUsersService: Partial<UsersService>;
  let service: AuthService;
  beforeEach(async () => {
    const users: User[] = [];
    fakeUsersService = {
      find: (email: string) => {
        const filteredUsers = users.filter((user) => user.email === email);
        return Promise.resolve(filteredUsers);
      },
      create: (email: string, password: string) => {
        const newUser = {
          id: Math.floor(Math.random() * 999999),
          email,
          password,
        } as User;
        users.push(newUser);
        return Promise.resolve(newUser);
      },
    };
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUsersService,
        },
      ],
    }).compile();
    service = module.get(AuthService);
  });

  it('can create instance', () => {
    expect(service).toBeDefined();
  });

  it('should hash password during signup', async () => {
    const user = await service.signup('test@mail.com', 'pass');

    expect(user.password).not.toEqual('pass');
    expect(user.password.split('.').length).toBe(2);
  });

  it('throws an error when signup with existing email', async () => {
    await service.signup('test@mail.com', 'pass');
    await expect(service.signup('test@mail.com', 'pass')).rejects.toThrow(
      BadRequestException,
    );
  });

  it('throws an error when signin called with not existing email', async () => {
    await expect(service.signin('test@mail.com', 'pass')).rejects.toThrow(
      NotFoundException,
    );
  });

  it('throws an error when signin called with bad password', async () => {
    await service.signup('test@mail.com', 'pass1');
    await expect(service.signin('test@mail.com', 'pass')).rejects.toThrow(
      ForbiddenException,
    );
  });

  it('returns user if good password is provided', async () => {
    await service.signup('laskdjf@alskdfj.com', 'pass');
    const user = await service.signin('laskdjf@alskdfj.com', 'pass');
    expect(user).toBeDefined();
  });
});
