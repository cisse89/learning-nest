import { Report } from '../reports/report.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: false})
  isAdmin: boolean;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Report, (report) => report.user)
  reports: Report[];

  @AfterInsert()
  logInsert() {
    console.log('Inserted user: ', this.id);
  }

  @AfterUpdate()
  logUpdate() {
    console.log('Updated user: ', this.id);
  }

  @AfterRemove()
  logRemove() {
    console.log('Removed user: ', this.id);
  }
}
