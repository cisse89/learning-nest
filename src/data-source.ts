import { DataSource, DataSourceOptions } from 'typeorm';

console.log(process.env.NODE_ENV)

export const appDataSource = new DataSource({
  type: 'sqlite',
  database: 'db.sqlite',
  entities: ['**/*.entity.ts'],
  migrations: [__dirname + '/migrations/*.ts'],
} as DataSourceOptions);