import { IsNumber, IsString, Min, Max, IsLongitude, IsLatitude } from 'class-validator';

export class CreateReportDto {
  @IsNumber()
  price: number;

  @IsString()
  make: string;

  @IsString()
  model: string;

  @Min(1999)
  @Max(2022)
  @IsNumber()
  year: number;

  @IsLongitude()
  lng: number;

  @IsLatitude()
  lat: number;

  @Min(0)
  @Max(10000000)
  @IsNumber()
  mileage: number;
}
