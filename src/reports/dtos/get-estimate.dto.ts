import { IsNumber, IsString, Min, Max, IsLongitude, IsLatitude } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetEstimateDto {
  @IsString()
  make: string;

  @IsString()
  model: string;

  @Transform(({value})=>parseInt(value))
  @Min(1999)
  @Max(2022)
  @IsNumber()
  year: number;

  @Transform(({value})=>parseFloat(value))
  @IsLongitude()
  lng: number;

  @Transform(({value})=>parseFloat(value))
  @IsLatitude()
  lat: number;

  @Transform(({value})=>parseInt(value))
  @Min(0)
  @Max(10000000)
  @IsNumber()
  mileage: number;
}
