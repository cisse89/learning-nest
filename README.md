## Description

Project created during course on Udemy [NestJS: The Complete Developer's Guide](https://www.udemy.com/course/nestjs-the-complete-developers-guide/)

## Installation

```bash
$ npm install
```

## Running the app
Preppare `.env.development` and `.env.test` file with 
```
DB_NAME=<name for sqlite database>
COKIE_KEY=<string to encrypt cookie>
```

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

